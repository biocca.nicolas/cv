# My LaTeX CV repository

This repository contains the LaTeX source code and other necessary files to create and customize my curriculum vitae. Additionally, I have created a specific LaTeX class tailored for this purpose to simplify customization and maintain consistency in formatting.

## Getting Started

To get started, you can clone this repository to your local machine using the following command:

```bash
git clone https://gitlab.com/biocca.nicolas/cv.git
```

## Customizing the CV

1. Open the `my_data_cv.tex` file located in the `src` directory with your favorite LaTeX editor.

2. Edit the CV's contents to match your personal information, including your name, contact details, education, work experience, skills, and any additional sections you wish to include.

3. For details regarding experience and education, refer to the `main.tex` file in the `src` directory. 

4. Details on layout and formatting implementation can be found in the `myCV.cls` file in the `src` directory.

## Compiling the CV

A `Makefile` is available in the `src` directory.

```bash
make all
```

Running this command will generate a PDF file named `cv.pdf` in the same directory.

## CV preview 

<div align="center">
  <img src="https://gitlab.com/biocca.nicolas/cv/-/raw/main/src/cv.jpg" alt="sample cv" width="400">
</div>


## Attributions

- `twentysecondcv` class by Carmine Spagnuolo: [TwentySecondsCurriculumVitae-LaTex](https://github.com/spagnuolocarmine/TwentySecondsCurriculumVitae-LaTex)
- `fortysecodscv` class by René Wirnata: [FortySecondsCV](https://github.com/PandaScience/FortySecondsCV)

## License

This project is licensed under the MIT License - see the [LICENSE](https://opensource.org/license/mit/) file
